import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs'; 

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'octopus-assignment';
  public data : any;
  constructor(private http: HttpClient) {
    this.getJSON().subscribe(data => {
      if (data){
        this.data = data;
        console.log(this.data);
      }

    });
    console.log(this.data);
  }

  public getJSON(): Observable<any> {
    return this.http.get("./assets/data.json");
  }
}
