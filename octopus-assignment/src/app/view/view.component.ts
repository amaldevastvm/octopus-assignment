import { Component, OnInit, Input } from '@angular/core';
import { AppService } from '../../app/app.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit {

  public data_dict : any;
  constructor(private service: AppService, private router: Router) { 
    
    this.service.getData().subscribe(data=>{
      if(data){
        this.data_dict = data;
      }
    });
  }
  public selectedRow(index:any){
    this.router.navigate(['/detail/' + index]);
  }
  ngOnInit(): void{
  }

}
