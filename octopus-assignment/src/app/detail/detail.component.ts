import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import { AppService } from '../../app/app.service';
import { LocationStrategy } from '@angular/common';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit {

  public entity : any;

  constructor(private route: ActivatedRoute, private service: AppService, private location: LocationStrategy,  private router: Router) { 
    // history.pushState(null, "null", window.location.href);  
    this.location.onPopState(() => {
      document.location.href = document.location.origin;
      // history.pushState(null, "null", window.location.href)
    });
  }

  ngOnInit(): void {
    this.route.params.subscribe(param =>
      this.entity = this.getDetailsById(param['id']));
  }

  private getDetailsById(id:number){
    this.service.getData().subscribe(data=>{
      if(data && data[id]){
        this.entity = data[id];
      }
    });

  }
}
